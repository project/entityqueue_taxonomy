/**
 * Created by bappasarkar on 2/2/17.
 */
(function($) {
    Drupal.behaviors.entityqueueTaxonomy = {
        attach: function(context, settings) {
            // Update autocomplete path if we have check any term ref field.
            $("div[id^='edit-taxonomy-fields'] .form-checkbox").change(function(){
                setTaxonomyField();
            });

            // Hide taxonomy text field if we didn't choose any term reference field.
            setTaxonomyField();

            function setTaxonomyField() {
                var selected = [];
                $("div[id^='edit-taxonomy-fields'] input:checked").each(function() {
                    selected.push($(this).attr('value'));
                });
                if (selected.length === 0) {
                    $('div.tid').hide();
                    $("input[id^='edit-add-another']").hide();
                }
                else {
                    $('div.tid').show();
                    $("input[id^='edit-add-another']").show();
                }

                var url = document.location.origin + '/entityqueue_taxonomy/taxonomy/autocomplete/' + selected.join(',');

                // Update autocomplete path
                $('.tid input.autocomplete').removeClass('autocomplete-processed').val(url);
                $('.tid input.form-autocomplete').unbind();
                try{
                    Drupal.behaviors.autocomplete.attach(document);
                }catch (e){
                    //console.log(e);
                }


            }
        }
    };
})(jQuery);
