<?php

/**
 * @file
 * Entityqueue handler for scheduled queues.
 */

$plugin = array(
  'title' => t('Taxonomy queue'),
  'class' => 'EntityQueueTaxonomyEntityQueueHandler',
  'weight' => -99,
  'queue type' => 'multiple',
);
