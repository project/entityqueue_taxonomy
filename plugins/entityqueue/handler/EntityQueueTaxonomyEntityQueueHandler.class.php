<?php

/**
 * @file
 * Contains Entityqueue handler for scheduled queues.
 */

/**
 * A scheduled queue implementation.
 */
class EntityQueueTaxonomyEntityQueueHandler extends EntityQueueHandlerBase {

  /**
   * Overrides EntityQueueHandlerBase::settingsForm().
   */
  public function settingsForm() {
    return array();
  }

  /**
   * Overrides EntityQueueHandlerBase::subqueueForm().
   */
  public function subqueueForm(EntitySubqueue $subqueue, &$form_state) {
    $form_state['storage']['tids'] = isset($form_state['storage']['tids']) ? $form_state['storage']['tids'] :
    (isset($subqueue->subqueue_id) ? count($subqueue->data['taxonomy']['tids']) : 1);
    $values = isset($form_state['values']) ? $form_state['values'] : (array) $subqueue;

    $form = [];
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Subqueue label'),
      '#required' => TRUE,
      '#default_value' => isset($values['label']) ? $values['label'] : '',
    );

    $form['name'] = array(
      '#type' => 'machine_name',
      '#title' => t('Subqueue name'),
      '#required' => TRUE,
      '#default_value' => isset($values['name']) ? $values['name'] : '',
      '#machine_name' => array(
        'exists' => 'entityqueue_subqueue_load',
        'source' => array('label'),
      ),
      '#disabled' => (isset($subqueue->subqueue_id)),
    );
    $queue = entityqueue_queue_load($subqueue->queue);
    $entity_type = $queue->target_type;
    $bundles = $queue->settings['target_bundles'];
    $data = field_read_fields(array('type' => 'taxonomy_term_reference'));
    $fields = $data;
    $data = array_keys($data);
    $data = field_read_instances(array(
      'field_name' => $data,
      'entity_type' => $entity_type,
      'bundle' => $bundles,
    ));
    $data = array_column($data, 'field_name');
    $data = array_unique($data);
    $taxonomy_ref_fields = [];
    foreach ($data as $t) {
      $temp = array_column($fields[$t]['settings']['allowed_values'], 'vocabulary');
      $temp = implode(', ', $temp);
      $taxonomy_ref_fields[$t] = $t . ' from vocabulary: <strong>' . $temp . '</strong>';
    }
    if (!empty($subqueue->data['taxonomy']['fields'])) {
      $default_fields = $subqueue->data['taxonomy']['fields'];
    }
    else {
      reset($taxonomy_ref_fields);
      $default_fields = key($taxonomy_ref_fields);
    }
    $form['taxonomy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy settings'),
      '#collapsible' => TRUE,
      '#collapsed' => (isset($subqueue->subqueue_id)),
      '#prefix' => '<div id="taxonomy">',
      '#suffix' => '</div>',
    );
    $form['taxonomy']['taxonomy_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Taxonomy fields'),
      '#required' => TRUE,
      '#position' => 'left' ,
      '#options' => $taxonomy_ref_fields,
      '#default_value' => $default_fields,
      '#description' => t('Select taxonomy field from where you want to relate the entity in the queue.'),
      '#disabled' => (isset($subqueue->subqueue_id)),
    );
    $form['taxonomy']['tid']['#tree'] = TRUE;
    for ($i = 0; $i < $form_state['storage']['tids']; $i++) {
      // Create an auto complete text field to add a term from the vocabulary
      // you selected above.
      $form['taxonomy']['tid'][$i] = array(
        '#type' => 'textfield',
        '#title' => t('Taxonomy'),
        '#description' => t('Type the taxonomy'),
        '#required' => TRUE,
        '#default_value' => (!empty($subqueue->data['taxonomy']['tids'][$i]['tid'])) ? $subqueue->data['taxonomy']['tids'][$i]['tid_str'] . ' - ' . $subqueue->data['taxonomy']['tids'][$i]['tid'] : NULL,
        '#autocomplete_path' => 'entityqueue_taxonomy/taxonomy/autocomplete/' . implode(',', $default_fields),
        '#maxlength' => 1024,
        '#disabled' => (isset($subqueue->subqueue_id)),
        '#prefix' => '<div class="tid">',
        '#suffix' => '</div>',
      );
    }
    $form['taxonomy']['add_another'] = array(
      '#type' => 'button',
      '#value' => t('Add another taxonomy'),
      '#href' => '',
      '#ajax' => array(
        'callback' => 'entityqueue_taxonomy_tid_add_more_callback',
        'wrapper' => 'taxonomy',
      ),
      '#disabled' => (isset($subqueue->subqueue_id)),
    );

    // Assign the validate function.
    $form['#validate'][] = 'entityqueue_taxonomy_subqueue_form_validate';
    // Attach the js file in the form.
    if (!isset($form['#attached']['js'])) {
      $form['#attached']['js'] = array();
    }
    array_push($form['#attached']['js'], drupal_get_path('module', 'entityqueue_taxonomy') . '/js/entityqueue_taxonomy.js');

    // Will not allow to add another tid after the subqueue got saved.
    if (!(isset($subqueue->subqueue_id))) {
      $form_state['storage']['tids']++;
    }
    return $form;
  }

  /**
   * Overrides EntityQueueHandlerBase::canDeleteSubqueue().
   */
  public function canDeleteSubqueue(EntitySubqueue $subqueue) {
    if ($this->isLiveQueue($subqueue)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Overrides EntityQueueHandlerBase::loadFromCode().
   */
  public function loadFromCode() {
    $this->ensureSubqueue();
  }

  /**
   * Overrides EntityQueueHandlerBase::insert().
   */
  public function insert() {
    $this->ensureSubqueue();
  }

  /**
   * Determines if a subqueue is the live subqueue.
   */
  public function isLiveQueue(EntitySubqueue $subqueue) {
    return $subqueue->name == $this->queue->name . '__live';
  }

  /**
   * Makes sure that every queue has a subqueue.
   */
  protected function ensureSubqueue() {
    return;
  }

}
